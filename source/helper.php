﻿﻿<?php
/**
 * Bibelvers-Klasse für das Bibelversanzeige! Joomla-Modul
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http:// docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * @license        GNU/GPL, see LICENSE.php
 * mod_bibelversanzeige is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */


class Begruessung {
	
	// Eigenschaften der Klasse
	public $Mitteilung; // 
	
	
	// Die Klasse instanziieren, d.h. ein Objekt erzeugen
	// function __construct(&$params) {	
	function __construct() {	
			
			// Die Lokale Urzeit holen
			date_default_timezone_set('Europe/Berlin');
			$localtime = localtime();
			// $localtime = array('','12','0');
			// print_r($localtime);
			switch ($localtime[2]) {
				case 21: case 22: case 23: case 0: case 1: case 2: case 3: case 4:
					$Mitteilung = "Hallo!";
					break;
				case 5: case 6: case 7: case 8: case 9: 
					$Mitteilung = "Guten Morgen!";
					break;
				case 10: case 11: case 12:
					$Mitteilung = "Guten Tag!";
					break;
				case 13: case 14: case 15: case 16:
					$Mitteilung = "Guten Nachmittag!";
					break;
				case 17: case 18: case 19: case 20:
					$Mitteilung = "Guten Abend!";
					break;
			}
				
			$this->Mitteilung = $Mitteilung; 
	}
}
?>